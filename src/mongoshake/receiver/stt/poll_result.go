package sst

import(
	"fmt"
	// "reflect"
	"strings"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"mongoshake/oplog"

	LOG "github.com/vinllen/log4go"
	"github.com/vinllen/mgo/bson"
)

/** 
* 投票结果 
*
**/
func Poll_Result_Handle(ol *oplog.PartialLog, db *sql.DB, base *sql.DB) {
	fmt.Println("----------------------* " + ol.Namespace[15:] + " *-----------------")
	if ol.Operation == "i"{

		do_poll_result_insert(ol, db, base)

	} else if ol.Operation == "u"{
		LOG.Info("-------------- poll_result update -----------------")
		do_poll_result_update(ol,  db, base)

	} else if ol.Operation == "d"{

		do_poll_result_delete(ol, db)

	} else {
		fmt.Println(ol.Operation)
		fmt.Println("\tQuery : ", ol.Query)
		fmt.Println("\tObject : ", ol.Object)
	}
}


/*****************************************************************
 		更新 
*****************************************************************/
func do_poll_result_update(ol *oplog.PartialLog, db *sql.DB, base *sql.DB) {
	query := ol.Query["_id"]
	qId, ok := query.(bson.ObjectId)
	if !ok {
		fmt.Println("--> [poll - u]断言失败：", ol.Query)
	}
	id := qId.Hex()
	// o := ol.Object.Map()
	fmt.Println("更新 poll_result : ", id)
	fmt.Println(ol.Object)

	// set := o["$set"]
	// if set != nil {
	// 	do_poll_result_update_part(id, set, db, base)
	// }else{
	// 	do_poll_result_update_all(id, ol, db, base)
	// }
}


/*****************************************************************
 		插入 
*****************************************************************/
func do_poll_result_insert(ol *oplog.PartialLog, db *sql.DB, base *sql.DB) {
	objectRaw, err := bson.Marshal(ol.Object)
	PrintError("", err)
	pr := new(Poll_Result)
	if err := bson.Unmarshal(objectRaw, pr); err != nil {
		LOG.Crashf("unmarshal oplog[%v] failed[%v]", objectRaw, err)
		return
	}
	fmt.Println("插入 : ", pr.Id.Hex())

	// 开启事务
	tx, err := db.Begin()
	PrintError("--> [poll_result - i] tx err : ", err)
	defer tx.Rollback()

	poll_id 	:= pr.PollId.Hex()
	isSubmit 	:= 0
	students 	:= pr.Students
	mongo_id 	:= pr.Id.Hex()
	exam_type 	:= 4
	ae_id, subject := GetAeId(poll_id, db)
	for _, student := range students {
		stuId 	:= student.StuId.Hex()
		user_no := GetUserNo(stuId, base)
		classid := GetClassId(user_no, base)

		// app_exam_student
		stmt, err := tx.Prepare("INSERT INTO app_exam_student(user_no, classid, ae_id, mongo_id, exam_type, isSubmit, del) VALUES(?, ?, ?, ?, ?, ?, 0)")
		PrintError("--> [poll_result - i] app_exam_student prepare failed : ", err)
		defer stmt.Close()
		es_res, err := stmt.Exec(user_no, classid, ae_id, mongo_id, exam_type, isSubmit)
		PrintError("--> [poll_result - i] insert app_exam_student failed : ", err)
		aes_id, err := es_res.LastInsertId()
		PrintError("--> [poll_result - i] get app_exam_student LastInsertId failed : ", err)

		results := student.Answer.Result
		body_id, ti_id, right_answer, qu_type := GetInfoFromPoll(poll_id, db)

		var ans string
		if qu_type == 2 { // 多选题，将答案拼接在一起
			ans = strings.Join(results, "")
		} else { // 其他，只取最后一个
			ans = results[len(results)-1]
		}
		status := 1
		var marking int
		if ans != right_answer {	// 不正确
			marking = 1
		}else {				// 正确
			marking = 2
		}
		// app_exam_result
		stmt, err = tx.Prepare("INSERT INTO app_exam_result(answer, right_answer, marking, body_id, qu_type, ti_id, status, aes_id, del) VALUES(?,?,?,?,?,?,?,?,0)")
		PrintError("--> [poll_result - i] app_exam_result prepare failed : ", err)
		_, err = stmt.Exec(ans, right_answer, marking, body_id, qu_type, ti_id, status, aes_id)
		PrintError("--> [poll_result - i] insert app_exam_result failed : ", err)
		// 如果是错题，加入到 app_favorites 表中
		if marking == 1 {
			stmt, err = tx.Prepare("INSERT INTO app_favorites(user_no, body_id, ti_id, type, master, subject) VALUES(?, ?, ?, 0, 0, ?)")
			PrintError("--> [poll_result - i] app_exam_result prepare failed : ", err)
			_,	err = stmt.Exec(user_no, body_id, ti_id, subject)
			PrintError("--> [poll_result - i] insert app_exam_result failed : ", err)
		}
	}
	// 提交事务
	err = tx.Commit()
	PrintError("--> [poll_result - i] tx commit err : ", err)
	fmt.Println("--> 插入成功")
}


/*****************************************************************
 		删除 
*****************************************************************/
func do_poll_result_delete(ol *oplog.PartialLog, db *sql.DB) {
	objectRaw, err := bson.Marshal(ol.Object)
	if err != nil{
		fmt.Println(err)
	}
	objId := new(ObjId)
	if err := bson.Unmarshal(objectRaw, objId); err != nil {
		LOG.Crashf("unmarshal oplog[%v] failed[%v]", objectRaw, err)
		return
	}

	id := objId.Id.Hex()
	fmt.Println("删除 : ", id)

	tx, err := db.Begin()
	PrintError("--> [poll_result - d] tx err : ", err)
	defer tx.Rollback()

	// app_exam_result
	stmt, err := tx.Prepare("UPDATE app_exam_result SET `del`=1 WHERE aes_id in (SELECT aes_id FROM app_exam_student WHERE mongo_id=?)")
 	PrintError("--> [poll_result - d] app_exam prepared failed : ", err)
 	defer stmt.Close()
 	_, err = stmt.Exec(id)
 	PrintError("--> [poll_result - d] app_exam update failed : ", err)

 	// app_exam_result
	stmt, err = tx.Prepare("UPDATE app_exam_student SET `del`=1 WHERE mongo_id=?")
 	PrintError("--> [poll_result - d] app_exam prepared failed : ", err)
 	_, err = stmt.Exec(id)
 	PrintError("--> [poll_result - d] app_exam update failed : ", err)

	err = tx.Commit()
	PrintError("--> [poll_result - d] tx commit err : ", err)
	fmt.Println("--> 删除成功")
}


// 打印
func printPr(pr *Poll_Result) {
	fmt.Println("id : \t\t", 		pr.Id.Hex())
	fmt.Println("clientRes : \t", 	pr.ClientRes)
	fmt.Println("cAPollId : \t", 	pr.CApollId)
	fmt.Println("pollId : \t", 		pr.PollId)
	fmt.Println("source : \t", 		pr.Source)
	fmt.Println("grades : \t", 		pr.Grades)
	fmt.Println("period : \t", 		pr.Period)
	fmt.Println("term: \t\t", 		pr.Term)
	fmt.Println("startTime : \t", 	pr.StartTime)
	fmt.Println("endTime : \t", 	pr.EndTime)
	fmt.Println("cn : \t\t", 		pr.Cn)
	fmt.Println("userType : \t", 	pr.UserType)
	fmt.Println("uploadTime : \t", 	pr.UploadTime)
	fmt.Println("students : \t")
	if pr.Students != nil {
		for i, s := range pr.Students {
			fmt.Println("--> 第 ", i, " 个学生：")
			fmt.Println("\t stuId: ", 		s.StuId.Hex())
			fmt.Println("\t groupId: ", 	s.GroupId)
			fmt.Println("\t media: ", 		s.Media)
			fmt.Println("\t answerEndTime: ", s.AnswerEndTime)
			fmt.Println("\t time: ", 		s.Time)
			fmt.Println("\t answer: ", 		*s.Answer)
			fmt.Println("\t result : ", 	s.Answer.Result)
			fmt.Println("\t answersLog: ", 	s.AnswersLog)
		}
	}
	fmt.Println("pollSummary : \t", 	pr.PollSummary)
	fmt.Println("name : \t", 			pr.Name)
	fmt.Println("type : \t", 			pr.Type)
	fmt.Println("createTime : \t", 		pr.CreateTime)
	fmt.Println("updateTIme : \t", 		pr.UpdateTime)
	fmt.Println("del : \t\t", 			pr.Del)
	fmt.Println("version : \t",			pr.Version)
	fmt.Println("classes : \t", 		pr.Classes.Id.Hex())
	fmt.Println("banCi : \t", 			pr.BanCi.Id.Hex())
	fmt.Println("user : \t", 			pr.User.Id.Hex())
}


/*****************************************************************
		部分更新		(暂时没发现这一块的业务逻辑，先注释掉)
*****************************************************************/
// func do_poll_result_update_part(id string, set interface{}, db *sql.DB, base *sql.DB) {
// 	s := set.(bson.D)
// 	m := s.Map()
// 	// 开启事务
// 	tx, err := db.Begin()
// 	PrintError("--> [poll - i] tx err : ", err)
// 	defer tx.Rollback()

// 	for k := range m {
// 		switch k {
		// 从业务逻辑的操作上来看，不会更新这个字段
		// case "pollId":
		// 	fmt.Println(k, " : ", m[k])

		// case "del":
		// 	fmt.Println(k, " : ", m[k])
			// del := m[k]
			// // app_exam_student
			// stmt, err := tx.Prepare("UPDATE `app_exam_student` SET `del`=? WHERE `mongo_id`=?")
			// PrintError("--> [exam_result - u] app_exam_student prepare failed : ", err)
			// defer stmt.Close()
			// _,  err = stmt.Exec(del, id)
			// PrintError("--> [exam_result - u] update app_exam_student failed : ", err)
			// // app_exam_result
			// stmt, err = tx.Prepare("UPDATE app_exam_result SET `del`=? WHERE `aes_id`=(SELECT aes_id FROM app_exam_student WHERE mongo_id=?)")
			// PrintError("--> [exam_result - u] app_exam_result prepare failed : ", err)
			// _,	err = stmt.Exec(del, id)
			// PrintError("--> [exam_result - u] update app_exam_result failed : ", err)

		// case "students":
		// 	fmt.Println(k, " : ", m[k])
			// students := (m[k]).([]interface {})
			// for _, s := range students {
			// 	stu_d := s.(bson.D)
			// 	stu_m := stu_d.Map()
			// 	objId := stu_m["stuId"]
			// 	oId := objId.(bson.ObjectId)
			// 	stu_id := oId.Hex()

			// 	user_no 	:= GetUserNo(stu_id, base)
			// 	classid 	:= GetClassId(user_no, base)

			// 	stmt, err := tx.Prepare("UPDATE `app_exam_student` SET `classid`=? WHERE `mongo_id`=? AND `user_no`=?")
			// 	PrintError("--> [exam_result - u] app_exam_student prepare failed : ", err)
			// 	defer stmt.Close()
			// 	_,  err = stmt.Exec(classid, id, user_no)
			// 	PrintError("--> [exam_result - u] update app_exam_student failed : ", err)

			// 	answers := stu_m["answers"]
			// 	ans := answers.([]interface {})
			// 	for _, an := range ans {
			// 		an_d := an.(bson.D)
			// 		an_m := an_d.Map()
			// 		ti_id := (an_m["tiId"]).(string)
			// 		fmt.Println(ti_id, " : ", reflect.TypeOf(ti_id))
			// 		qu_num := an_m["no"]
			// 		results := (an_m["results"]).([]interface {})
			// 		body_id, right_answer, qu_score, qu_type := GetInfoFromExam(id, ti_id, db)
			// 		var result string
			// 		if qu_type == 2 { // 多选题，将答案拼接在一起
			// 			for _, r := range results {
			// 				r_s := r.(string)
			// 				result += r_s
			// 			}
			// 			// result = strings.Join(results, "")
			// 		} else { // 其他，只取最后一个
			// 			result = (results[len(results)-1]).(string)
			// 		}
			// 		isR := (an_m["isRight"]).(bson.D)
			// 		isR_m := isR.Map()
			// 		isRight := isR_m["key"]
			// 		var marking int
			// 		if isRight == 0 {
			// 			marking = 1
			// 		}else {
			// 			marking = 2
			// 		}
			// 		score := an_m["score"]
			// 		stmt, err = tx.Prepare("UPDATE app_exam_result SET `answer`=?, `right_answer`=?, `marking`=?, `total_score`=?, `score`=?, `body_id`=?, `qu_type`=?, `ti_id`=?, `qu_num`=? WHERE `aes_id`=(SELECT aes_id FROM app_exam_student WHERE mongo_id=? AND user_no=?)")
			// 		PrintError("--> [exam_result - u] app_exam_result prepare failed : ", err)
			// 		_,	err = stmt.Exec(result, right_answer, marking, qu_score, score, body_id, qu_type, ti_id, qu_num, id, user_no)
			// 		PrintError("--> [exam_result - u] update app_exam_result failed : ", err)
			// 	}
			// }

// 		default:
// 			fmt.Println(reflect.TypeOf(k), " : ", reflect.TypeOf(m[k]))
// 			fmt.Println(k, " : ", m[k])
// 		}
// 	}

// 	// 提交事务
// 	err = tx.Commit()
// 	PrintError("--> [poll - d] tx commit err : ", err)
// 	fmt.Println("--> 更新成功")
// }


/*****************************************************************
		全量更新
*****************************************************************/
// func do_poll_result_update_all(id string, ol *oplog.PartialLog, db *sql.DB, base *sql.DB) {
// 	objectRaw, err := bson.Marshal(ol.Object)
// 	PrintError("", err)
// 	pr := new(Poll_Result)
// 	if err := bson.Unmarshal(objectRaw, pr); err != nil {
// 		LOG.Crashf("unmarshal oplog[%v] failed[%v]", objectRaw, err)
// 		return
// 	}
// 	// 开启事务
// 	tx, err := db.Begin()
// 	PrintError("--> [poll_result - u] tx err : ", err)
// 	defer tx.Rollback()

// 	del 		:= pr.Del
// 	poll_id 	:= pr.PollId.Hex()
// 	ae_id 		:= GetAeId(poll_id, db)
// 	students 	:= pr.Students
// 	mongo_id 	:= pr.Id.Hex()
// 	for _, student := range students {
// 		stuId 	:= student.StuId.Hex()
// 		user_no := GetUserNo(stuId, base)
// 		classid := GetClassId(user_no, base)

// 		// app_exam_student
// 		stmt, err := tx.Prepare("UPDATE `app_exam_student` SET `classid`=?, `ae_id`=?, `del`=? WHERE mongo_id=? AND user_no=?")
// 		PrintError("--> [poll_result - u] app_exam_student prepare failed : ", err)
// 		defer stmt.Close()
// 		_, err = stmt.Exec(classid, ae_id, del, mongo_id, user_no)
// 		PrintError("--> [poll_result - u] insert app_exam_student failed : ", err)

// 		results := student.Answer.Result
// 		isRight := student.Answer.IsRight.Key
// 		var marking int
// 		if isRight == 0 {	// 不正确
// 			marking = 1
// 		}else {
// 			marking = 2
// 		}
// 		ti_id, right_answer, qu_type := GetInfoFromPoll(poll_id, db)
// 		var ans string
// 		if qu_type == 2 { // 多选题，将答案拼接在一起
// 			ans = strings.Join(results, "")
// 		} else { // 其他，只取最后一个
// 			ans = results[len(results)-1]
// 		}
// 		// app_exam_result
// 		stmt, err = tx.Prepare("UPDATE app_exam_result SET `answer`=?, `right_answer`=?, `marking`=?, `qu_type`=?, `ti_id`=?, `del`=? WHERE `aes_id`=(SELECT aes_id FROM app_exam_student WHERE mongo_id=? AND user_no=?)")
// 		PrintError("--> [poll_result - u] app_exam_result prepare failed : ", err)
// 		_, err = stmt.Exec(ans, right_answer, marking, qu_type, ti_id, del, mongo_id, user_no)
// 		PrintError("--> [poll_result - u] insert app_exam_result failed : ", err)
// 	}

// 	// 提交事务
// 	err = tx.Commit()
// 	PrintError("--> [poll_result - u] tx commit err : ", err)
// 	fmt.Println("--> 更新成功")
// }