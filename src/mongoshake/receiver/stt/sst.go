package sst

import (
	"time"
	"github.com/vinllen/mgo/bson"
)


/** (string, bson.ObjectId)
	{_id: ObjectIdHex("5d302866f22e618b8cdc8c2d")}
**/
type ObjId struct {
	Id	bson.ObjectId 	`bson:"_id"`
}


/** {int: string} 
	{整形： 字符串}
**/
type ISMap struct {
	Key		int		`bson:"key"`
	Text	string	`bson:'text"`
}

/** {string: string} 
	{字符串： 字符串}
**/
type SSMap struct {
	Key		string	`bson:"key"`
	Text	string	`bson:"text"`
}

/** {string: ObjectId}
	对象的索引
**/
type Ref struct {
	Ref	string			`bson:"$ref"`
	Id	bson.ObjectId	`bson:"$id"`
}

/*****************************************************************
 		投票 
*****************************************************************/
type Poll struct {
	Id		bson.ObjectId	`bson:"_id"`
	BodyId 		string 		`bson:"bodyId"`
	Source		*ISMap		`bson:"source"`
	Type		*ISMap		`bson:"type"`
	Name		string		`bson:"name"`
	Topic		string		`bson:"topic"`
	Options		[]*SSMap	`bson:"options"`
	Answers		string		`bson:"answers"`
	Duration	int			`bson:"duration"`
	IsAnonymous	*ISMap		`bson:"isAnonymous"`
	Period		*ISMap		`bson:"period"`
	Term 		*ISMap		`bson:"term"`
	UserType	*ISMap		`bson:"userType"`
	Classify	*ISMap		`bson:"classify"`
	CreateTime	time.Time	`bson:"createTime"`
	UpdateTime	time.Time	`bson:"updateTime"`
	Del			int			`bson:"del"`
	Version		int			`bson:"version"`
	User		*Ref		`bson:"user"`
	AnswMnt		int			`bson:"answMnt"`
	IsAllLower	*ISMap		`bson:"isAllLower"`
	IsModify	int			`bson:"isModify"`
	ClientRes	string		`bson:"clientRes"`
	CPollId		string		`bson:"cPollId"`
	PollLs		*Poll_Ls	`bson:"pollLs"`
	UploadTime	time.Time	`bson:"uploadTime"`
	GridFsId	string		`bson:"gridFsId"`
}

/** poll 中的 PollLs **/
type Poll_Ls struct {
	Grade		*ISMap	`bson:"grade"`
	BussType	*ISMap	`bson:"bussType"`
	Classes		*Ref	`bson:"classes"`
	BanCi		*Ref	`bson:"banCi"`
}


/*****************************************************************
 		投票结果 
*****************************************************************/
type Poll_Result struct {
	Id			bson.ObjectId	`bson:"_id"`
	ClientRes	string			`bson:"clientRes"`
	CApollId	string			`bson:"cApollId"`
	PollId		bson.ObjectId	`bson:"pollId"`
	Source		*ISMap			`bson:"source"`
	Grades		*ISMap			`bson:"grades"`
	Period		*ISMap			`bson:"period"`
	Term 		*ISMap			`bson:"term"`
	StartTime	time.Time		`bson:"startTime"`
	EndTime		time.Time		`bson:"endTime"`
	Cn			int				`bson:"cn"`
	UserType	*ISMap			`bson:"userType"`
	UploadTime	time.Time		`bson:"uploadTime"`
	Students	[]*Pr_Student	`bson:"students"`
	PollSummary	*PollSummaryType	`bson:"pollSummary"`
	Name		string			`bson:"name"`
	Type		*ISMap			`bson:"type"`
	CreateTime	time.Time		`bson:"createTime"`
	UpdateTime	time.Time		`bson:"updateTime"`
	Del			int				`bson:"del"`
	Version		int				`bson:"version"`
	Classes		*Ref			`bson:"classes"`
	BanCi		*Ref			`bson:"banCi"`
	User		*Ref			`bson:"user"`
}

/** poll_result 对象中的 "pollSummary" **/
type PollSummaryType struct {
	Cn			int			`bson:"cn"`
	CnName		string		`bson:"cnName"`
	StuCn		int			`bson:"stuCn"`
	ZqStuCn		int			`bson:"zqStuCn"`
	ZqRate		string		`bson:"zqRate"`
	Time		int32		`bson:"time"`
	StartTime	time.Time	`bson:"startTime"`
}

/** Poll_Result 中的 "students.answer" **/
type Pr_Student_Answer struct {
	Result		[]string	`bson:"result"`
	IsRight		*ISMap		`bson:"isRight"`
	Paths		bson.M		`bson:"paths"`
}

/** poll_result 中的 "students" **/
type Pr_Student struct {
	StuId			bson.ObjectId		`bson:"stuId"`
	GroupId			bson.ObjectId		`bson:"groupId"`
	Media			bson.M				`bson:"media"`
	AnswerEndTime	time.Time			`bson:"answerEndTime"`
	Time			int32				`bson:"time"`
	Answer			*Pr_Student_Answer	`bson:"answer"`
	AnswersLog		bson.M 				`bson:"answersLog"`
}



/*****************************************************************
 		测验
*****************************************************************/
type Exam struct {
	Id			bson.ObjectId	`bson:"_id"`
	Source		*ISMap			`bson:"source"`
	Name		string			`bson:"name"`
	Duration	int				`bson:"duration"`
	LowScore	float32			`bson:"lowScore"`
	Page		*Exam_Page		`bson:"page"`
	Detail		[]*Exam_Detail	`bson:"detail"`
	Period		*ISMap			`bson:"period"`
	Term		*ISMap			`bson:"term"`
	UserType	*ISMap			`bson:"userType"`
	PageId		string			`bson:"pageId"`
	IsModify	int				`bson:"isModify"`
	CreateTime	time.Time		`bson:"createTime"`
	UpdateTime	time.Time		`bson:"updateTime"`
	Del			int				`bson:"del"`
	Version		int				`bson:"version"`
	User		*Ref			`bson:"user"`
}

/** Exam 中的 page **/
type Exam_Page struct {
	TpName		string 		`bson:"tpName"`
	CourseName	string 		`bson:"courseName"`
	SumScore	float32		`bson:"sumScore"`
	QuNum		int 		`bson:"quNum"`
	Degree		*ISMap		`bson:"degree"`
}

/** Exam 中的 detail **/
type Exam_Detail struct {
	Id			bson.ObjectId	`bson:"_id"`
	No			string			`bson:"no"`
	Content 	string 			`bson:"content"`
	Type		*ISMap			`bson:"type"`
	Score		float32			`bson:"score"`
	Answers		string			`bson:"answers"`
	DegreeLevel	*ISMap			`bson:"degreeLevel"`
	Option 		[]*SSMap		`bson:"option"`
	KnowPoint	string			`bson:"knowPonit"`
	BodyId 		string 			`bson:"bodyId"`
}


/*****************************************************************
 		测验结果
*****************************************************************/
type Exam_Result struct {
	Id			bson.ObjectId	`bson:"_id"`
	ClientRes	string			`bson:"clientRes"`
	CAexamId	string			`bson:"cAexamId"`
	ExamId		bson.ObjectId	`bson:"examId"`
	Source		*ISMap			`bson:"source"`
	Grades		*ISMap			`bson:"grades"`
	Period		*ISMap			`bson:"period"`
	Term		*ISMap			`bson:"term"`
	StartTime	time.Time		`bson:"startTime"`
	EndTime		time.Time		`bson:"endTime"`
	UserType	*ISMap			`bson:"userType"`
	UploadTime	time.Time		`bson:"uploadTime"`
	Students 	[]*Er_Student 	`bson:"students"`
	ExamSummary	*Exam_Summary	`bson:"examSummary"`
	Name		string			`bson:"name"`
	QuNum 		int 			`bson:"quNum"`
	ZqRate 		float32 		`bson:"zqRate"`
	CreateTime	time.Time		`bson:"createTime"`
	UpdateTime	time.Time		`bson:"updateTime"`
	Del			int				`bson:"del"`
	Version		int				`bson:"version"`
	Classes		*Ref			`bson:"classes"`
	BanCi		*Ref			`bson:"banCi"`
	User		*Ref			`bson:"user"`
}

/** Exam_Result 中的 students.answers **/
type Er_Student_Answer struct {
	TiId 	string 		`bson:"tiId"`
	No 		string 		`bson:"no"`
	Results	[]string 	`bson:"results"`
	IsRight	*ISMap		`bson:"isRight"`
	Score 	float32 	`bson:"score"`
	Paths 	bson.M 		`bson:"paths"`
	Media 	bson.M 		`bson:"media"`
}

/** Exam_Result 中的 Students **/
type Er_Student struct {
	StuId			bson.ObjectId	`bson:"stuId"`
	AnswerEndTime	time.Time		`bson:"answerEndTime"`
	TotalScore 		float32			`bson:"totalScore"`
	Answers 		[]*Er_Student_Answer	`bson:"answers"`
	AnswersLog 		bson.M 			`bson:"answersLog"`
	CreateTime		time.Time		`bson:"createTime"`
	UpdateTime		time.Time		`bson:"updateTime"`
	Del				int				`bson:"del"`
}

/** Exam_Result 中的 ExamSummary **/
type Exam_Summary struct {
	StuCn		int			`bson:"stuCn"`
	QuNum		int			`bson:"quNum"`
	TotalScore	float32		`bson:"totalScore"`
	Average		float32 	`bson:"average"`
	Time		int32		`bson:"time"`
	StartTime	time.Time	`bson:"startTime"`
}