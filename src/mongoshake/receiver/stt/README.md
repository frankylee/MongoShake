# MongoShake

## 一、安装 MongoDB， 单节点启动 oplog

### 1，docker 安装 mongodb

```shell
# 启动脚本
$ docker run --detach \
     --name mongo-standalone1\
     --publish 3369:27017 \		# 绑定主机端口 3369
     --volume /data/mongo-test/db:/data/db \
     --volume /data/mongo-test/configdb:/data/configdb \
     -e MONGO_INITDB_ROOT_USERNAME=root \
     -e MONGO_INITDB_ROOT_PASSWORD=123456 \
     mongo:3.4.18 \
     --logpath /data/configdb/mongo.log \
     --auth \
     --clusterAuthMode "keyFile" \
     --keyFile /data/configdb/keyfile \
     --bind_ip "0.0.0.0" \
     --replSet rs		# 开启副本集，名字 rs
```

### 2，进入容器中，完成初始化

```shell
# 1，进入容器
$ docker exec -it mongo-standalone1 /bin/bash

# 2，尝试连接 mongo，这是会报错
$ mongo localhost:27017/admin -u root -p sititong123987
{
"ok" : 0,
"errmsg" : "not master and slaveOk=false",
"code" : 13435,
"codeName" : "NotMasterNoSlaveOk"
}

# 3，初始化
> rs.initiate({ _id: "rs", members: [{_id:0, host:"127.0.0.1:27017"}]})

# 初始完，副本集中唯一的节点，可能短时间显示为SECONDARY或OTHER。一般而言，稍等一会，就会自然恢复为primary，无需人工干预。
# 此时，在 local 库中，就能够看到有 oplog.rs 集合了。
```



## 二、安装 Go

```shell
# 1，下载
$ cd /usr/local && wget https://dl.google.com/go/go1.12.6.linux-amd64.tar.gz

# 2，解压
$ tar -zxvf go1.12.6.linux-amd64.tar.gz

# 3，添加到 PATH 中
$ vi /etc/profile	# 在文件末尾添加
	export GO_HOME=/usr/local/go
	export PATH=$PATH:$GO_HOME/bin
	
# 4，下载 govendor，这是一个go工程的依赖包管理工具
$ go get -u github.com/kardianos/govendor

# 如果这个命令下载不成功，直接从 github 上下载
$ cd /usr/local/go/src && mkdir -p github.com/kardianos/ && cd github.com/kardianos/
$ git clone https://github.com/kardianos/govendor.git
$ go get -u github.com/kardianos/govendor
# 这是在命令行输入 govendor，就可以发现下载成功了
```





## 三、下载 MongoShake

```shell
$ cd /home && git clone https://github.com/alibaba/MongoShake.git	# 下载源码
$ cd MongoShake/
$ export GOPATH=`pwd`
$ cd src/vendor
$ govendor sync		# 下载工程依赖
$ cd ../../ && ./build.sh
$ ./bin/collector -conf=conf/collector.conf		# 启动 mongodb 连接程序，运行前需先修改配置文件
$ ./bin/receiver -conf=conf/receiver.conf		# 启动接收程序，运行前需先修改配置文件
```







































