package sst

import(
	"fmt"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"mongoshake/oplog"

	LOG "github.com/vinllen/log4go"
	"github.com/vinllen/mgo/bson"
)

/*****************************************************************
 	从 s_teacher 表中查询 subject
*****************************************************************/
func GetSubject(user_no string, base *sql.DB) int {
	stmt, err := base.Prepare("SELECT `subject` FROM `s_teacher` WHERE `user_no` = ? ")
	PrintError("--> getSubject() Prepare failed : ",err)
	defer stmt.Close()
	var subject int
	err = stmt.QueryRow(user_no).Scan(&subject)
	PrintError("--> getSubject() QueryRow failed : ", err)
	return subject
}

/*****************************************************************
 	从 s_user 表中查找 user_name, user_no 两个字段值
*****************************************************************/
func GetUser(user_id string, base *sql.DB) (string, string) {
	stmt, err := base.Prepare("SELECT `user_name`, `user_no` FROM `s_user` WHERE `mongo_id`= ? ")
	PrintError("--> getUser() Prepare failed : ", err)
	defer stmt.Close()
	var user_no 	string
	var user_name 	string
	err = stmt.QueryRow(user_id).Scan(&user_name, &user_no)
	PrintError("--> getUser() QueryRow failed : ", err)
	return user_name, user_no
}

/*****************************************************************
 	通过 mongo_id 查询学生的 user_no
*****************************************************************/
func GetUserNo(stuId string, base *sql.DB) string {
	var user_no string
	stmt, err := base.Prepare("SELECT user_no FROM s_student WHERE mongo_id=?")
	PrintError("--> s_student prepare failed : ", err)
	defer stmt.Close()
	err = stmt.QueryRow(stuId).Scan(&user_no)
	PrintError("--> select s_student failed : ", err)
	return user_no
}

/*****************************************************************
 	查找学生用户对应的 班级ID
*****************************************************************/
func GetClassId(user_no string, base *sql.DB) int {
	var classId int
	stmt, err := base.Prepare("SELECT sc_id FROM s_class_relationship WHERE user_no=?")
	PrintError("--> s_class_relationship prepare failed : ", err)
	defer stmt.Close()
	err = stmt.QueryRow(user_no).Scan(&classId)
	PrintError("--> select s_class_relationship failed : ", err)
	return classId
}

/*****************************************************************
 	从 exam 中查找需要的信息
*****************************************************************/
func GetInfoFromExam(page_id string, ti_id string, db *sql.DB) (int, string, int, int) {
	stmt, err := db.Prepare("SELECT body_id, answers, score, qu_type FROM app_stt_page_detail WHERE page_id=? AND ti_id=?")
	PrintError("--> app_stt_page_detail prepare failed : ", err)
	defer stmt.Close()
	var score 	int
	var answers string
	var body_id int
	var qu_type int
	err = stmt.QueryRow(page_id, ti_id).Scan(&body_id, &answers, &score, &qu_type)
	PrintError("--> select app_stt_page_detail failed : ", err)
	return body_id, answers, score, qu_type
}


/*****************************************************************
 	从 exam 中查找需要的信息
*****************************************************************/
func GetInfoFromPoll(page_id string, db *sql.DB) (int, string, string, int) {
	stmt, err := db.Prepare("SELECT body_id, ti_id, answers, qu_type FROM app_stt_page_detail WHERE page_id = ? ")
	PrintError("--> app_stt_page_detail prepare failed : ", err)
	defer stmt.Close()
	var body_id	int
	var ti_id 	string
	var answers string
	var qu_type int
	err = stmt.QueryRow(page_id).Scan(&body_id, &ti_id, &answers, &qu_type)
	PrintError("--> select app_stt_page_detail failed : ", err)
	return body_id, ti_id, answers, qu_type
}



/*****************************************************************
 	从 app_exam 表中获取 ae_id
*****************************************************************/
func GetAeId(exam_id string, db *sql.DB) (int, int) {
	var ae_id int
	var sub_id int
	stmt, err := db.Prepare("SELECT ae_id, sub_id FROM app_exam WHERE stt_page_id=?")
	PrintError("--> app_exam prepare failed : ", err)
	defer stmt.Close()
	err = stmt.QueryRow(exam_id).Scan(&ae_id, &sub_id)
	PrintError("--> select app_exam failed : ", err)
	return ae_id, sub_id
}


/*****************************************************************
 	查找章节/知识点关联关系表，如果该题的关联关系已经存在就返回true，否则返回false
*****************************************************************/
func IsExist(body_id int, db *sql.DB) bool {
	exist := false
	var count int
	err := db.QueryRow("SELECT COUNT(*) FROM m_question_body_kd_relationship WHERE body_id=? AND del=0", body_id).Scan(&count)
	PrintError("--> select count(*) m_question_body_kd_relationship from app failed : ", err)
	if count > 0 {
		exist = true
	}
	return exist
}


/*****************************************************************
 	处理在线题库试题的章节 / 知识点关联关系
*****************************************************************/
func DoQuKpRelation(body_id int, db *sql.DB, tk *sql.DB, tx *sql.Tx) {
	exist := IsExist(body_id, db)
	// 如果关联关系已经存在，就不用重复插入了
	if !exist {
		var (
			bus_type int
			bus_id int
			bus_code string
		)
		// 从在线题库查找 body_id 关联的章节、知识点信息
		rows, err := tk.Query("SELECT bus_type, bus_id, bus_code FROM m_question_body_kd_relationship WHERE body_id=? AND del=0", body_id)
		PrintError("--> Query m_question_body_kd_relationship from tk error : ", err)
		defer rows.Close()
		for rows.Next() {
			err = rows.Scan(&bus_type, &bus_id, &bus_code)
			PrintError("--> Scan m_question_body_kd_relationship from tk error : ", err)

			stmt, err := tx.Prepare("INSERT INTO m_question_body_kd_relationship(body_id, bus_type, bus_id, bus_code, del) VALUES(?, ?, ?, ?, 0)")
			PrintError("--> m_question_body_kd_relationship prepared failed : ", err)
			defer stmt.Close()
			_, err = stmt.Exec(body_id, bus_type, bus_id, bus_code)
			PrintError("--> m_question_body_kd_relationship insert failed : ", err)
		}
		// 从在线题库 m_question_body_relationship 查找 body_id 关联的 question
		rows, err = tk.Query("SELECT qu_id FROM m_question_body_relationship WHERE qb_id=? AND del=0", body_id)
		PrintError("--> Query m_question_body_relationship from tk error : ", err)
		for rows.Next() {
			var qu_id int
			err = rows.Scan(&qu_id)
			PrintError("--> Scan m_question_body_relationship from tk error : ", err)

			quRelationRows, err := tk.Query("SELECT bus_type, bus_id, bus_code FROM m_question_relationship WHERE qu_id=? AND del=0", qu_id)
			PrintError("--> Query m_question_relationship from tk failed : ", err)
			defer quRelationRows.Close()
			for quRelationRows.Next() {
				err = quRelationRows.Scan(&bus_type, &bus_id, &bus_code)
				PrintError("--> Scan m_question_relationship from tk error : ", err)

				stmt, err := tx.Prepare("INSERT INTO m_question_relationship(qu_id, bus_type, bus_id, bus_code, del) VALUES(?, ?, ?, ?, 0)")
				PrintError("--> m_question_relationship prepared failed : ", err)
				defer stmt.Close()
				_, err = stmt.Exec(qu_id, bus_type, bus_id, bus_code)
				PrintError("--> m_question_relationship insert failed : ", err)
			}

		}

	}

}


/*****************************************************************
 	同一打印错误信息
*****************************************************************/
func PrintError(msg string, err error) {
	if err != nil {
		fmt.Println(msg, err)
	}
}


/*****************************************************************
 	
*****************************************************************/
func GetQueryId(ol *oplog.PartialLog) string {
	queryRaw, err := bson.Marshal(ol.Query)
	PrintError("", err)
	objId := new(ObjId)
	if err := bson.Unmarshal(queryRaw, objId); err != nil {
		LOG.Crashf("unmarshal oplog[%v] failed[%v]", queryRaw, err)
		fmt.Println("转换文档ID失败！")
		return ""
	}
	return objId.Id.Hex()
}