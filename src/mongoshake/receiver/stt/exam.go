package sst

import(
	"fmt"
	"reflect"
	"strconv"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"mongoshake/oplog"

	LOG "github.com/vinllen/log4go"
	"github.com/vinllen/mgo/bson"
)

/** 
* 测验 
* 
**/
func Exam_Handler(ol *oplog.PartialLog, db *sql.DB, base *sql.DB) {
	// 在线题库
	tk, err := sql.Open("mysql", "jonyuan:jonyuan135246!@#@tcp(39.106.108.171)/online_exercises_lib")
	if err != nil {
		LOG.Info(err)
	}
	defer tk.Close()
	fmt.Println("----------------------* " + ol.Namespace[15:] + " *-----------------")
	if ol.Operation == "i"{

		do_exam_insert(ol, db, base, tk)

	} else if ol.Operation == "u"{

		do_exam_update(ol, db, base, tk)

	} else if ol.Operation == "d"{

		do_exam_delete(ol, db)

	} else {
		fmt.Println(ol.Operation)
		fmt.Println("\tQuery : ", ol.Query)
		fmt.Println("\tObject : ", ol.Object)
	}
}


/*****************************************************************
 		更新 
*****************************************************************/
func do_exam_update(ol *oplog.PartialLog, db *sql.DB, base *sql.DB, tk *sql.DB){
	query := ol.Query["_id"]
	qId, ok := query.(bson.ObjectId)
	if !ok {
		fmt.Println("--> [exam - u]断言失败：", ol.Query)
	}
	id := qId.Hex()
	o := ol.Object.Map()
	fmt.Println("更新 : ", id)

	set := o["$set"]
	if set != nil {
		do_exam_update_part(id, set, db, base, tk)
	}else{
		do_exam_update_all(id, ol, db, base, tk)
	}
}


/*****************************************************************
		部分更新
*****************************************************************/
func do_exam_update_part(id string, set interface{}, db *sql.DB, base *sql.DB, tk *sql.DB) {
	s := set.(bson.D)
	m := s.Map()
	// 开启事务
	tx, err := db.Begin()
	PrintError("--> [poll - i] tx err : ", err)
	defer tx.Rollback()

	for k := range m {
		switch k {
		case "pageId":
			fmt.Println(k, " : ", m[k])
			pageId := (m[k]).(int)
			// app_exam
			stmt, err 	:= tx.Prepare("UPDATE app_exam SET `page_id`=? WHERE stt_page_id=?")
			PrintError("--> [exam - u] app_exam prepared failed : ", err)
			defer stmt.Close()
			_, err 		 = stmt.Exec(pageId, id)
		 	PrintError("--> [exam - u] app_exam update failed : ", err)

		case "name":
			fmt.Println(k, " : ", m[k])
			ae_name := m[k]
			// app_exam
			stmt, err 	:= tx.Prepare("UPDATE app_exam SET `ae_name`=? WHERE stt_page_id=?")
			PrintError("--> [exam - u] app_exam prepared failed : ", err)
			defer stmt.Close()
			_, err 		 = stmt.Exec(ae_name, id)
		 	PrintError("--> [exam - u] app_exam update failed : ", err)
			// app_stt_page
			stmt, err 	= tx.Prepare("UPDATE app_stt_page SET name=? WHERE page_id=?")
			PrintError("--> [exam - u] app_stt_page prepared failed : ", err)
			_, err 		= stmt.Exec(ae_name, id)
			PrintError("--> [exam - u] app_stt_page update failed : ", err)

		case "user":
			fmt.Println(k, " : ", m[k])
			user_d := (m[k]).(bson.D)
			user_m := user_d.Map()
			objId := user_m["$id"]
			oId := objId.(bson.ObjectId)
			user_id := oId.Hex()
			user_name, user_no 	:= GetUser(user_id, base)
			subjectId 			:= GetSubject(user_no, base)
			// app_exam
			stmt, err 	:= tx.Prepare("UPDATE app_exam SET `create_user_name`=?, `create_user_no`=?, `sub_id`=? WHERE stt_page_id=?")
			PrintError("--> [exam - u] app_exam prepared failed : ", err)
			defer stmt.Close()
			_, err 		 = stmt.Exec(user_name, user_no, subjectId, id)
			PrintError("--> [exam - u] app_exam update failed : ", err)
			// app_stt_page
			stmt, err 	= tx.Prepare("UPDATE app_stt_page SET sub_id=? WHERE page_id=?")
			PrintError("--> [exam - u] app_stt_page prepared failed : ", err)
			_, err 		= stmt.Exec(subjectId, id)
			PrintError("--> [exam - u] app_stt_page update failed : ", err)

		case "period":
			fmt.Println(k, " : ", m[k])
			year_D := (m[k]).(bson.D)
			year_m := year_D.Map()
			year := year_m["key"]
			// app_stt_page
			stmt, err 	:= tx.Prepare("UPDATE app_stt_page SET year=? WHERE page_id=?")
			PrintError("--> [exam - u] app_stt_page prepared failed : ", err)
			defer stmt.Close()
			_, err 		= stmt.Exec(year, id)
			PrintError("--> [exam - u] app_stt_page update failed : ", err)

		case "lowScore":
			fmt.Println(k, " : ", m[k])
			lowscore := m[k]
			// app_stt_page
			stmt, err 	:= tx.Prepare("UPDATE app_stt_page SET lowscore=? WHERE page_id=?")
			PrintError("--> [exam - u] app_stt_page prepared failed : ", err)
			defer stmt.Close()
			_, err 		= stmt.Exec(lowscore, id)
			PrintError("--> [exam - u] app_stt_page update failed : ", err)

		case "del":
			fmt.Println(k, " : ", m[k])
			del := m[k]
			// app_exam
			stmt, err 	:= tx.Prepare("UPDATE `app_exam` SET `del`=? WHERE `stt_page_id`=?")
			PrintError("--> [poll - u] app_exam prepared failed : ", err)
			defer stmt.Close()
			_, err = stmt.Exec(del, id)
			PrintError("--> [poll - u] app_exam update failed : ", err)
		 	// app_stt_page
			stmt, err = tx.Prepare("UPDATE `app_stt_page` SET `del`=? WHERE page_id=?")
			PrintError("--> [poll - u] app_stt_page prepared failed : ", err)
			_, err = stmt.Exec(del, id)
			PrintError("--> [poll - u] app_stt_page update failed : ", err)
			// app_stt_page_detail
			stmt, err = tx.Prepare("UPDATE `app_stt_page_detail` SET `del`=? WHERE `page_id`=?")
			PrintError("--> [poll - u] app_stt_page_detail prepared failed : ", err)
			_, err = stmt.Exec(del, id)
			PrintError("--> [poll - u] app_stt_page_detail update failed : ", err)
			// app_stt_ti
			stmt, err = tx.Prepare("UPDATE `app_stt_ti` SET `del`=? WHERE `ti_id`=?")
			PrintError("--> [poll - u] app_stt_ti prepared failed : ", err)
			_, err = stmt.Exec(del, id)
			PrintError("--> [poll - u] app_stt_ti update failed : ", err)
			// app_stt_item
			stmt, err = tx.Prepare("UPDATE `app_stt_item` SET `del`=? WHERE `ti_id`=?")
			PrintError("--> [poll - u] app_stt_item prepared failed : ", err)
			_, err = stmt.Exec(del, id)
			PrintError("--> [poll - u] app_stt_item update failed : ", err)

		case "page":
			fmt.Println(k, " : ", m[k])
			page_d := (m[k]).(bson.D)
			page_m := page_d.Map()
			quNum := page_m["quNum"]
			sumScore := page_m["sumScore"]
			// app_stt_page
			stmt, err := tx.Prepare("UPDATE app_stt_page SET total_score=?, total_num=? WHERE page_id=?")
			PrintError("--> [exam - u] app_stt_page prepared failed : ", err)
			defer stmt.Close()
			_, err = stmt.Exec(sumScore, quNum, id)
			PrintError("--> [exam - u] app_stt_page update failed : ", err)

		case "detail":
			fmt.Println(k, " : ", m[k])
			details := (m[k]).([]interface {})
			for _, d := range details {
				dt_d := d.(bson.D)
				dt_m := dt_d.Map()
				obj_id := dt_m["_id"]
				o_id := obj_id.(bson.ObjectId)
				ti_id := o_id.Hex()
				score := dt_m["score"]
				order := dt_m["no"]
				answers := dt_m["answers"]
				type_d := (dt_m["type"]).(bson.D)
				type_m := type_d.Map()
				qu_type := type_m["key"]
				body_id := (dt_m["bodyId"]).(int)
				// app_stt_page_detail
				stmt, err := tx.Prepare("UPDATE app_stt_page_detail SET `body_id`=?, `score`=?, `order`=?, `answers`=?, `qu_type`=? WHERE `page_id`=? AND `ti_id`=?")
				PrintError("--> [exam - u] app_stt_page_detail prepared failed : ", err)
				defer stmt.Close()
				_, err = stmt.Exec(body_id, score, order, answers, qu_type, id, ti_id)
				PrintError("--> [exam - u] app_stt_page_detail update failed : ", err)

				if body_id == 0 {
					ti_name := dt_m["content"]
					// app_stt_ti
					stmt, err = tx.Prepare("UPDATE app_stt_ti SET ti_name=?, ti_answers=? WHERE ti_id=?")
					PrintError("--> [exam - u] app_stt_ti prepared failed : ", err)
					_, err 	= stmt.Exec(ti_name, answers, ti_id)
					PrintError("--> [exam - u] app_stt_ti insert failed : ", err)
					if dt_m["option"] != nil {
						options := (dt_m["option"]).([]interface{})
						for _, opt := range options {
							op_d := opt.(bson.D)
							op_m := op_d.Map()
							key 	:= op_m["key"]
							content := op_m["text"]
							// app_stt_item
							stmt, err = tx.Prepare("UPDATE app_stt_item SET `key`=?, `content`=? WHERE ti_id=?")
							PrintError("--> [exam - u] app_stt_item prepared failed : ", err)
							_, err 	= stmt.Exec(key, content, ti_id)
							PrintError("--> [exam - u] app_stt_item insert failed : ", err)
						}
					}
				} else {
					// 处理在线题库试题的 章节/知识点 关联关系
					DoQuKpRelation(body_id, db, tk, tx)
				}
			}
		default:
			fmt.Println(reflect.TypeOf(k), " : ", reflect.TypeOf(m[k]))
			fmt.Println(k, " : ", m[k])
		}
	}

	// 提交事务
	err = tx.Commit()
	PrintError("--> [poll - d] tx commit err : ", err)
	fmt.Println("--> 更新成功")
}


/*****************************************************************
		全量更新
*****************************************************************/
func do_exam_update_all(id string, ol *oplog.PartialLog, db *sql.DB, base *sql.DB, tk *sql.DB) {
	objectRaw, err := bson.Marshal(ol.Object)
	PrintError("", err)
	exam := new(Exam)
	if err := bson.Unmarshal(objectRaw, exam); err != nil {
		LOG.Crashf("unmarshal oplog[%v] failed[%v]", objectRaw, err)
		return
	}
	// 开启事务
	tx, err := db.Begin()
	PrintError("--> [exam - u] tx err : ", err)
	defer tx.Rollback()

	var pageId int
	if exam.PageId == "" {
		pageId = 0
	}else{
		pageId, _ = strconv.Atoi(exam.PageId)
	}
	del 				:= exam.Del
	ae_name 			:= exam.Name
	user_id 			:= exam.User.Id.Hex()
 	user_name, user_no 	:= GetUser(user_id, base)
 	subjectId 			:= GetSubject(user_no, base)
 	// app_exam
	stmt, err 	:= tx.Prepare("UPDATE app_exam SET `page_id`=?, `ae_name`=?, `create_user_name`=?, `create_user_no`=?, `sub_id`=?, del=? WHERE stt_page_id=?")
	PrintError("--> [exam - u] app_exam prepared failed : ", err)
	defer stmt.Close()
	_, err 		 = stmt.Exec(pageId, ae_name, user_name, user_no, subjectId, del, id)
 	PrintError("--> [exam - u] app_exam update failed : ", err)

	year 	 := exam.Period.Key
	quNum 	 := exam.Page.QuNum
	lowscore := exam.LowScore
	sumScore := exam.Page.SumScore
	// app_stt_page
	stmt, err 	= tx.Prepare("UPDATE app_stt_page SET name=?, lowscore=?, year=?, sub_id=?, total_score=?, total_num=?, del=? WHERE page_id=?")
	PrintError("--> [exam - u] app_stt_page prepared failed : ", err)
	_, err 		= stmt.Exec(ae_name, lowscore, year, subjectId, sumScore, quNum, del, id)
	PrintError("--> [exam - u] app_stt_page update failed : ", err)

	details := exam.Detail
	for _, detail := range details{
		ti_id 	:= detail.Id.Hex()
		score 	:= detail.Score
		order 	:= detail.No
		answers := detail.Answers
		qu_type := detail.Type.Key
		var body_id int
		if detail.BodyId == "" {
			body_id = 0
		}else {
			body_id, _ = strconv.Atoi(detail.BodyId)
		}
		// app_stt_page_detail
		stmt, err = tx.Prepare("UPDATE app_stt_page_detail SET `body_id`=?, `score`=?, `order`=?, `answers`=?, `qu_type`=?, `del`=? WHERE `page_id`=? AND `ti_id`=?")
		PrintError("--> [exam - u] app_stt_page_detail prepared failed : ", err)
		_, err = stmt.Exec(body_id, score, order, answers, qu_type, del, id, ti_id)
		PrintError("--> [exam - u] app_stt_page_detail update failed : ", err)

		if body_id == 0 {
			ti_name := detail.Content
			// app_stt_ti
			stmt, err = tx.Prepare("UPDATE app_stt_ti SET ti_name=?, ti_answers=?, del=? WHERE ti_id=?")
			PrintError("--> [exam - u] app_stt_ti prepared failed : ", err)
			_, err 	= stmt.Exec(ti_name, answers, del, ti_id)
			PrintError("--> [exam - u] app_stt_ti insert failed : ", err)

			options := detail.Option
			for _, option := range options {
				key 	:= option.Key
				content := option.Text
				// app_stt_item
				stmt, err = tx.Prepare("UPDATE app_stt_item SET `key`=?, `content`=?, `del`=? WHERE ti_id=?")
				PrintError("--> [exam - u] app_stt_item prepared failed : ", err)
				_, err 	= stmt.Exec(key, content, del, ti_id)
				PrintError("--> [exam - u] app_stt_item insert failed : ", err)
			}
		} else {
			// 处理在线题库试题的 章节/知识点 关联关系
			DoQuKpRelation(body_id, db, tk, tx)
		}
	}
	// 提交事务
	err = tx.Commit()
	PrintError("--> [exam-d] tx commit err : ", err)
	fmt.Println("--> 更新成功")
}


/*****************************************************************
 		插入 
*****************************************************************/
func do_exam_insert(ol *oplog.PartialLog, db *sql.DB, base *sql.DB, tk *sql.DB){
	fmt.Println("插入")
	objectRaw, err := bson.Marshal(ol.Object)
	PrintError("", err)
	exam := new(Exam)
	if err := bson.Unmarshal(objectRaw, exam); err != nil {
		LOG.Crashf("unmarshal oplog[%v] failed[%v]", objectRaw, err)
		return
	}

	// 开启事务
	tx, err := db.Begin()
	PrintError("--> [exam-d] tx err : ", err)
	defer tx.Rollback()

	fmt.Println("_id : ", exam.Id.Hex())
	var pageId int
	if exam.PageId == "" {
		pageId = 0
	}else{
		pageId, _ = strconv.Atoi(exam.PageId)
	}
	ae_name 			:= exam.Name
	user_id 			:= exam.User.Id.Hex()
 	stt_page_id 		:= exam.Id.Hex()
 	exam_status 		:= 2
 	join_classes_type 	:= 0		// 所带班级
 	user_name, user_no 	:= GetUser(user_id, base)
 	subjectId 			:= GetSubject(user_no, base)

	// app_exam
	stmt, err 	:= tx.Prepare("INSERT INTO app_exam(page_id, stt_page_id, exam_status, join_classes_type, ae_name, create_user_name, create_user_no, sub_id, del) VALUES(?, ?, ?, ?, ?, ?, ?, ?, 0)")
 	PrintError("--> [exam-i] app_exam prepared failed : ", err)
 	_, err 		= stmt.Exec(pageId, stt_page_id, exam_status, join_classes_type, ae_name, user_name, user_no, subjectId)
 	PrintError("--> [exam-i] app_exam insert failed : ", err)

	year 	 := exam.Period.Key
	quNum 	 := exam.Page.QuNum
	lowscore := exam.LowScore
	sumScore := exam.Page.SumScore

	// app_stt_page
	stmt, err = tx.Prepare("INSERT INTO app_stt_page(page_id, name, lowscore, year, sub_id, total_score, total_num, del) VALUES(?, ?, ?, ?, ?, ?, ?, 0)")
	PrintError("--> [exam-i] app_stt_page prepared failed : ", err)
	_, err 	  = stmt.Exec(stt_page_id, ae_name, lowscore, year, subjectId, sumScore, quNum)
	PrintError("--> [exam-i] app_stt_page insert failed : ", err)

	details := exam.Detail
	for _, detail := range details{
		ti_id 	:= detail.Id.Hex()
		score 	:= detail.Score
		order 	:= detail.No
		answers := detail.Answers
		qu_type := detail.Type.Key
		var body_id int
		if detail.BodyId == "" {
			body_id = 0
		}else {
			body_id, _ = strconv.Atoi(detail.BodyId)
		}
		// app_stt_page_detail
		stmt, err = tx.Prepare("INSERT INTO app_stt_page_detail(`page_id`, `body_id`, `ti_id`, `score`, `order`, `answers`, `qu_type`, `del`) VALUES(?, ?, ?, ?, ?, ?, ?, 0)")
		PrintError("--> [exam-i] app_stt_page_detail prepared failed : ", err)
		_, err    = stmt.Exec(stt_page_id, body_id, ti_id, score, order, answers, qu_type)
		PrintError("--> [exam-i] app_stt_page_detail insert failed : ", err)

		// 如果 body_id 为空，说明是用户自己创建的试题，需要将试题保存到 app_stt_ti 和 app_stt_item 表
		if body_id == 0 {
			ti_name := detail.Content
			// app_stt_ti
			stmt, err = tx.Prepare("INSERT INTO app_stt_ti(ti_id, ti_name, ti_answers, del) VALUES(?, ?, ?, 0)")
			PrintError("--> [exam-i] app_stt_ti prepared failed : ", err)
			_, err 	  = stmt.Exec(ti_id, ti_name, answers)
			PrintError("--> [exam-i] app_stt_ti insert failed : ", err)
			options := detail.Option
			for _, option := range options {
				key 	:= option.Key
				content := option.Text
				// app_stt_item
				stmt, err := tx.Prepare("INSERT INTO app_stt_item(`ti_id`, `key`, `content`, `del`) VALUES(?, ?, ?, 0)")
				PrintError("--> [exam-i] app_stt_item prepared failed : ", err)
				_, err 	   = stmt.Exec(ti_id, key, content)
				PrintError("--> [exam-i] app_stt_item insert failed : ", err)
			}
		} else {
			// 处理在线题库试题的 章节/知识点 关联关系
			DoQuKpRelation(body_id, db, tk, tx)
		}
	}
	// 提交事务
	err = tx.Commit()
	PrintError("--> [exam-d] tx commit err : ", err)
	fmt.Println("--> 存储成功")
}


/*****************************************************************
 		删除 
*****************************************************************/
func do_exam_delete(ol *oplog.PartialLog, db *sql.DB){
	objectRaw, err := bson.Marshal(ol.Object)
	PrintError("", err)
	objId := new(ObjId)
	if err := bson.Unmarshal(objectRaw, objId); err != nil {
		LOG.Crashf("unmarshal oplog[%v] failed[%v]", objectRaw, err)
		return
	}
	id := objId.Id.Hex()
	fmt.Println("删除 : ", id)
	// 开启事务
	tx, err := db.Begin()
	PrintError("--> [exam-d] tx err : ", err)
	defer tx.Rollback()

	// app_exam
	stmt, err := tx.Prepare("UPDATE app_exam SET `del`=1 WHERE stt_page_id=?")
 	PrintError("--> [exam-d] app_exam prepared failed : ", err)
 	defer stmt.Close()
 	_, err = stmt.Exec(id)
 	PrintError("--> [exam-d] app_exam update failed : ", err)

 	// app_stt_page
	stmt, err = tx.Prepare("UPDATE app_stt_page SET `del`=1 WHERE page_id=?")
	PrintError("--> [exam-d] app_stt_page prepared failed : ", err)
	_, err = stmt.Exec(id)
	PrintError("--> [exam-d] app_stt_page insert failed : ", err)

	// app_stt_page_detail
	stmt, err = tx.Prepare("UPDATE app_stt_page_detail SET `del`=1 WHERE page_id=?")
	PrintError("--> [exam-d] app_stt_page_detail prepared failed : ", err)
	_, err = stmt.Exec(id)
	PrintError("--> [exam-d] app_stt_page_detail insert failed : ", err)

	// app_stt_ti
	stmt, err = tx.Prepare("UPDATE app_stt_ti SET `del`=1 WHERE ti_id in (SELECT ti_id FROM app_stt_page_detail WHERE page_id=?)")
	PrintError("--> [exam-d] app_stt_ti prepared failed : ", err)
	_, err = stmt.Exec(id)
	PrintError("--> [exam-d] app_stt_ti insert failed : ", err)

	// app_stt_item
	stmt, err = tx.Prepare("UPDATE app_stt_item SET del=1 WHERE ti_id in (SELECT ti_id FROM app_stt_page_detail WHERE page_id=?)")
	PrintError("--> [exam-d] app_stt_item prepared failed : ", err)
	_, err = stmt.Exec(id)
	PrintError("--> [exam-d] app_stt_item insert failed : ", err)
	// 提交事务
	err = tx.Commit()
	PrintError("--> [exam-d] tx commit err : ", err)
	fmt.Println("--> 删除成功")
}


// 打印对象中的所有属性
func printExam(exam *Exam) {
	fmt.Println("_id : ", 			exam.Id.Hex())
	fmt.Println("source : ", 		exam.Source)
	fmt.Println("name : ", 			exam.Name)
	fmt.Println("duration : ", 		exam.Duration)
	fmt.Println("lowScore : ", 		exam.LowScore)
	fmt.Println("page : ")
	fmt.Println("\t tpName : ", 	exam.Page.TpName)
	fmt.Println("\t courseName : ", exam.Page.CourseName)
	fmt.Println("\t sumScore : ", 	exam.Page.SumScore)
	fmt.Println("\t quNum : ", 		exam.Page.QuNum)
	fmt.Println("\t degree : ", 	exam.Page.Degree)
	fmt.Println("detail : ")
	if exam.Detail != nil {
		for i, detail := range exam.Detail{
			fmt.Println("第 ", i, " 题：")
			fmt.Println("\t id : ", 		detail.Id)
			fmt.Println("\t no : ", 		detail.No)
			fmt.Println("\t content : ", 	detail.Content)
			fmt.Println("\t type : ", 		detail.Type)
			fmt.Println("\t score : ", 		detail.Score)
			fmt.Println("\t answers : ", 	detail.Answers)
			fmt.Println("\t degreeLevel : ", detail.DegreeLevel)
			fmt.Println("\t option : ", 	detail.Option)
			fmt.Println("\t knowPoint : ", 	detail.KnowPoint)
			fmt.Println("\t bodyId : ", 	detail.BodyId)
		}
	}
	fmt.Println("period : ", 		exam.Period)
	fmt.Println("term : ", 			exam.Term)
	fmt.Println("userType : ", 		exam.UserType)
	fmt.Println("pageId : ", 		exam.PageId, reflect.TypeOf(exam.PageId))
	fmt.Println("isModify : ", 		exam.IsModify)
	fmt.Println("createTime : ", 	exam.CreateTime)
	fmt.Println("updateTime : ", 	exam.UpdateTime)
	fmt.Println("del : ", 			exam.Del)
	fmt.Println("version : ", 		exam.Version)
	fmt.Println("user : ", 			exam.User)
}