package sst

import(
	"fmt"
	// "reflect"
	"strings"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"mongoshake/oplog"

	LOG "github.com/vinllen/log4go"
	"github.com/vinllen/mgo/bson"
)

/** 
* 测验结果 
* 
**/
func Exam_Result_Handle(ol *oplog.PartialLog, db *sql.DB, base *sql.DB) {
	fmt.Println("----------------------* " + ol.Namespace[15:] + " *-----------------")
	if ol.Operation == "i"{

		do_exam_result_insert(ol, db, base)

	} else if ol.Operation == "u"{
		LOG.Info("-------------- exam_result update -----------------")
		do_exam_result_update(ol, db, base)

	} else if ol.Operation == "d"{

		do_exam_result_delete(ol, db)

	} else {
		fmt.Println(ol.Operation)
		fmt.Println("\tQuery : ", ol.Query)
		fmt.Println("\tObject : ", ol.Object)
	}
}



/*****************************************************************
 		更新 
*****************************************************************/
func do_exam_result_update(ol *oplog.PartialLog, db *sql.DB, base *sql.DB) {
	query := ol.Query["_id"]
	qId, ok := query.(bson.ObjectId)
	if !ok {
		fmt.Println("--> [exam_result - u]断言失败：", ol.Query)
	}
	id := qId.Hex()
	// o := ol.Object.Map()
	fmt.Println("更新 exam-result : ", id)
	fmt.Println(ol.Object)

	// set := o["$set"]
	// if set != nil {
	// 	do_exam_result_update_part(id, set, db, base)
	// }else{
	// 	do_exam_result_update_all(id, ol, db, base)
	// }
}



/*****************************************************************
 		插入 
*****************************************************************/
func do_exam_result_insert(ol *oplog.PartialLog, db *sql.DB, base *sql.DB) {
	objectRaw, err := bson.Marshal(ol.Object)
	PrintError("", err)
	er := new(Exam_Result)
	if err := bson.Unmarshal(objectRaw, er); err != nil {
		LOG.Crashf("unmarshal oplog[%v] failed[%v]", objectRaw, err)
		return
	}
	fmt.Println("插入 : ", er.Id)

	// 开启事务
	tx, err := db.Begin()
	PrintError("--> [exam_result - i] tx err : ", err)
	defer tx.Rollback()

	exam_id  	:= er.ExamId.Hex()
	ae_id, subject := GetAeId(exam_id, db)
	isSubmit 	:= 0
	mongo_id 	:= er.Id.Hex()
	students 	:= er.Students
	exam_type 	:= 3
	for _, student := range students {
		stuId := student.StuId.Hex()
		user_no := GetUserNo(stuId, base)
		totalScore := student.TotalScore
		classid := GetClassId(user_no, base)

		stmt, err := tx.Prepare("INSERT INTO app_exam_student(user_no, score, classid, ae_id, mongo_id, exam_type, isSubmit, del) VALUES(?, ?, ?, ?, ?, ?, ?, 0)")
		PrintError("--> [exam_result - i] app_exam_student prepare failed : ", err)
		defer stmt.Close()
		es_res,  err := stmt.Exec(user_no, totalScore, classid, ae_id, mongo_id, exam_type, isSubmit)
		PrintError("--> [exam_result - i] insert app_exam_student failed : ", err)
		aes_id,  err := es_res.LastInsertId()
		PrintError("--> [exam_result - i] get app_exam_student LastInsertId failed : ", err)

		answers := student.Answers
		for _, answer := range answers {
			var res 	string
			var marking int
			results := answer.Results
			score  := answer.Score
			qu_num := answer.No
			ti_id := answer.TiId
			body_id, right_answer, qu_score, qu_type := GetInfoFromExam(exam_id, ti_id, db)
			if qu_type == 2 { // 多选题，将答案拼接在一起
				res = strings.Join(results, "")
			} else { // 其他，只取最后一个
				res = results[len(results)-1]
			}
			status := 1
			if res != right_answer {	// 不正确
				marking = 1
			}else {				// 正确
				marking = 2
			}
			stmt, err = tx.Prepare("INSERT INTO app_exam_result(answer, right_answer, marking, total_score, score, body_id, qu_type, ti_id, status, qu_num, aes_id, del) VALUES(?,?,?,?,?,?,?,?,?,?,?,0)")
			PrintError("--> [exam_result - i] app_exam_result prepare failed : ", err)
			_,	err = stmt.Exec(res, right_answer, marking, qu_score, score, body_id, qu_type, ti_id, status, qu_num, aes_id)
			PrintError("--> [exam_result - i] insert app_exam_result failed : ", err)
			// 如果是错题，加入到 app_favorites 表中
			if marking == 1 {
				stmt, err = tx.Prepare("INSERT INTO app_favorites(user_no, body_id, ti_id, type, master, subject) VALUES(?, ?, ?, 0, 0, ?)")
				PrintError("--> [exam_result - i] app_exam_result prepare failed : ", err)
				_,	err = stmt.Exec(user_no, body_id, ti_id, subject)
				PrintError("--> [exam_result - i] insert app_exam_result failed : ", err)
			}
		}
	}
	// 提交事务
	err = tx.Commit()
	PrintError("--> [exam_result - i] tx commit err : ", err)
	fmt.Println("--> 更新成功")
}


/*****************************************************************
 		删除 
*****************************************************************/
func do_exam_result_delete(ol *oplog.PartialLog, db *sql.DB) {
	objectRaw, err := bson.Marshal(ol.Object)
	PrintError("", err)
	objId  := new(ObjId)
	if err := bson.Unmarshal(objectRaw, objId); err != nil {
		LOG.Crashf("unmarshal oplog[%v] failed[%v]", objectRaw, err)
		return
	}

	id := objId.Id.Hex()
	fmt.Println("删除 : ", id)
	// 开启事务
	tx, err := db.Begin()
	PrintError("--> [exam_result - d] tx err : ", err)
	defer tx.Rollback()

	// app_exam_result
	stmt, err 	:= tx.Prepare("UPDATE app_exam_result SET `del`=1 WHERE aes_id in (SELECT aes_id FROM app_exam_student WHERE mongo_id=?)")
 	PrintError("--> [exam_result - d] app_exam prepared failed : ", err)
 	defer stmt.Close()
 	_, err 		= stmt.Exec(id)
 	PrintError("--> [exam_result - d] app_exam update failed : ", err)

 	// app_exam_result
	stmt, err = tx.Prepare("UPDATE app_exam_student SET `del`=1 WHERE mongo_id=?")
 	PrintError("--> [exam_result - d] app_exam prepared failed : ", err)
 	_, err 	  = stmt.Exec(id)
 	PrintError("--> [exam_result - d] app_exam update failed : ", err)

	// 提交事务
 	err = tx.Commit()
	PrintError("--> [exam_result - d] tx commit err : ", err)
	fmt.Println("--> 删除成功")
}


// 打印
func printEr(er *Exam_Result) {
	fmt.Println("_id : ",		 er.Id)
	fmt.Println("clientRes : ",	 er.ClientRes)
	fmt.Println("cAexamId : ",	 er.CAexamId)
	fmt.Println("examId : ",	 er.ExamId)
	fmt.Println("source : ",	 er.Source)
	fmt.Println("grades : ",	 er.Grades)
	fmt.Println("period : ",	 er.Period)
	fmt.Println("term : ",		 er.Term)
	fmt.Println("startTime : ",	 er.StartTime)
	fmt.Println("endTime : ",	 er.EndTime)
	fmt.Println("userType : ",	 er.UserType)
	fmt.Println("uploadTime : ", er.UploadTime)
	fmt.Println("students : ")
	if er.Students != nil {
		for i, es := range er.Students{
			fmt.Println("第 ", i, " 个学生：")
			fmt.Println("\t stuId : ", 			es.StuId)
			fmt.Println("\t answerEndTime : ", 	es.AnswerEndTime)
			fmt.Println("\t totalScore : ", 	es.TotalScore)
			fmt.Println("\t answers : ")
			if es.Answers != nil {
				for j, ea := range es.Answers{
					fmt.Println("\t第 ", j, " 个答案：")
					fmt.Println("\t\t tiId : ", 	ea.TiId)
					fmt.Println("\t\t no : ", 		ea.No)
					fmt.Println("\t\t results : ", 	ea.Results)
					fmt.Println("\t\t isRight : ", 	ea.IsRight)
					fmt.Println("\t\t score : ", 	ea.Score)
					fmt.Println("\t\t paths : ", 	ea.Paths)
					fmt.Println("\t\t media : ", 	ea.Media)
				}
			}
			fmt.Println("\t answersLog : ", es.AnswersLog)
			fmt.Println("\t createTime : ", es.CreateTime)
			fmt.Println("\t updateTime : ", es.UpdateTime)
			fmt.Println("\t del : ", 		es.Del)
		}
	}
	fmt.Println("examSummary : ")
	if er.ExamSummary != nil {
		fmt.Println("\t stuCn : ", 		er.ExamSummary.StuCn)
		fmt.Println("\t quNum : ", 		er.ExamSummary.QuNum)
		fmt.Println("\t totalScore : ", er.ExamSummary.TotalScore)
		fmt.Println("\t average : ", 	er.ExamSummary.Average)
		fmt.Println("\t time : ", 		er.ExamSummary.Time)
		fmt.Println("\t startTime : ",	er.ExamSummary.StartTime)
	}
	fmt.Println("name : ", 			er.Name)
	fmt.Println("quNum : ", 		er.QuNum)
	fmt.Println("zqRate : ", 		er.ZqRate)
	fmt.Println("createTime : ", 	er.CreateTime)
	fmt.Println("updateTime : ", 	er.UpdateTime)
	fmt.Println("del : ", 			er.Del)
	fmt.Println("version : ", 		er.Version)
	fmt.Println("classes : ", 		er.Classes)
	fmt.Println("banCi : ", 		er.BanCi)
	fmt.Println("user : ", 			er.User)
} 



/*****************************************************************
		部分更新		(暂时没发现这一块的业务逻辑，先注释掉)
*****************************************************************/
// func do_exam_result_update_part(id string, set interface{}, db *sql.DB, base *sql.DB) {
// 	s := set.(bson.D)
// 	m := s.Map()
	// // 开启事务
	// tx, err := db.Begin()
	// PrintError("--> [poll - i] tx err : ", err)
	// defer tx.Rollback()

	// for k := range m {
	// 	switch k {
	// 	// 从业务逻辑的操作上来看，不会更新这个字段
	// 	case "examId":
	// 		fmt.Println(k, " : ", m[k])

	// 	case "del":
	// 		fmt.Println(k, " : ", m[k])
			// del := m[k]
			// // app_exam_student
			// stmt, err := tx.Prepare("UPDATE `app_exam_student` SET `del`=? WHERE `mongo_id`=?")
			// PrintError("--> [exam_result - u] app_exam_student prepare failed : ", err)
			// defer stmt.Close()
			// _,  err = stmt.Exec(del, id)
			// PrintError("--> [exam_result - u] update app_exam_student failed : ", err)
			// // app_exam_result
			// stmt, err = tx.Prepare("UPDATE app_exam_result SET `del`=? WHERE `aes_id`=(SELECT aes_id FROM app_exam_student WHERE mongo_id=?)")
			// PrintError("--> [exam_result - u] app_exam_result prepare failed : ", err)
			// _,	err = stmt.Exec(del, id)
			// PrintError("--> [exam_result - u] update app_exam_result failed : ", err)

		// case "students":
		// 	fmt.Println(k, " : ", m[k])
			// students := (m[k]).([]interface {})
			// for _, s := range students {
			// 	stu_d := s.(bson.D)
			// 	stu_m := stu_d.Map()
			// 	objId := stu_m["stuId"]
			// 	oId := objId.(bson.ObjectId)
			// 	stu_id := oId.Hex()

			// 	user_no 	:= GetUserNo(stu_id, base)
			// 	classid 	:= GetClassId(user_no, base)
			// 	totalScore 	:= stu_m["totalScore"]

			// 	stmt, err := tx.Prepare("UPDATE `app_exam_student` SET `score`=?, `classid`=? WHERE `mongo_id`=? AND `user_no`=?")
			// 	PrintError("--> [exam_result - u] app_exam_student prepare failed : ", err)
			// 	defer stmt.Close()
			// 	_,  err = stmt.Exec(totalScore, classid, id, user_no)
			// 	PrintError("--> [exam_result - u] update app_exam_student failed : ", err)

			// 	answers := stu_m["answers"]
			// 	ans := answers.([]interface {})
			// 	for _, an := range ans {
			// 		an_d := an.(bson.D)
			// 		an_m := an_d.Map()
			// 		ti_id := (an_m["tiId"]).(string)
			// 		fmt.Println(ti_id, " : ", reflect.TypeOf(ti_id))
			// 		qu_num := an_m["no"]
			// 		results := (an_m["results"]).([]interface {})
			// 		body_id, right_answer, qu_score, qu_type := GetInfoFromExam(id, ti_id, db)
			// 		var result string
			// 		if qu_type == 2 { // 多选题，将答案拼接在一起
			// 			for _, r := range results {
			// 				r_s := r.(string)
			// 				result += r_s
			// 			}
			// 			// result = strings.Join(results, "")
			// 		} else { // 其他，只取最后一个
			// 			result = (results[len(results)-1]).(string)
			// 		}
			// 		isR := (an_m["isRight"]).(bson.D)
			// 		isR_m := isR.Map()
			// 		isRight := isR_m["key"]
			// 		var marking int
			// 		if isRight == 0 {
			// 			marking = 1
			// 		}else {
			// 			marking = 2
			// 		}
			// 		score := an_m["score"]
			// 		stmt, err = tx.Prepare("UPDATE app_exam_result SET `answer`=?, `right_answer`=?, `marking`=?, `total_score`=?, `score`=?, `body_id`=?, `qu_type`=?, `ti_id`=?, `qu_num`=? WHERE `aes_id`=(SELECT aes_id FROM app_exam_student WHERE mongo_id=? AND user_no=?)")
			// 		PrintError("--> [exam_result - u] app_exam_result prepare failed : ", err)
			// 		_,	err = stmt.Exec(result, right_answer, marking, qu_score, score, body_id, qu_type, ti_id, qu_num, id, user_no)
			// 		PrintError("--> [exam_result - u] update app_exam_result failed : ", err)
			// 	}
			// }

	// 	default:
	// 		fmt.Println(reflect.TypeOf(k), " : ", reflect.TypeOf(m[k]))
	// 		fmt.Println(k, " : ", m[k])
	// 	}
	// }
	// 提交事务
	// err = tx.Commit()
	// PrintError("--> [poll - d] tx commit err : ", err)
	// fmt.Println("--> 更新成功")
// }


/*****************************************************************
		全量更新
*****************************************************************/
// func do_exam_result_update_all(id string, ol *oplog.PartialLog, db *sql.DB, base *sql.DB) {
	// objectRaw, err := bson.Marshal(ol.Object)
	// PrintError("", err)
	// er := new(Exam_Result)
	// if err := bson.Unmarshal(objectRaw, er); err != nil {
	// 	LOG.Crashf("unmarshal oplog[%v] failed[%v]", objectRaw, err)
	// 	return
	// }
	// 开启事务
	// tx, err := db.Begin()
	// PrintError("--> [poll_result - u] tx err : ", err)
	// defer tx.Rollback()

	// del 		:= er.Del
	// exam_id  	:= er.ExamId.Hex()
	// ae_id 		:= GetAeId(exam_id, db)
	// mongo_id 	:= er.Id.Hex()
	// students 	:= er.Students
	// for _, student := range students {
	// 	stuId 		:= student.StuId.Hex()
	// 	user_no 	:= GetUserNo(stuId, base)
	// 	classid 	:= GetClassId(user_no, base)
	// 	totalScore 	:= student.TotalScore

	// 	stmt, err := tx.Prepare("UPDATE `app_exam_student` SET `score`=?, `classid`=?, `ae_id`=?, `del`=? WHERE `mongo_id`=? AND `user_no`=?")
	// 	PrintError("--> [exam_result - u] app_exam_student prepare failed : ", err)
	// 	defer stmt.Close()
	// 	_,  err = stmt.Exec(totalScore, classid, ae_id, del, mongo_id, user_no)
	// 	PrintError("--> [exam_result - u] update app_exam_student failed : ", err)

	// 	answers := student.Answers
	// 	for _, answer := range answers {
	// 		var marking int
	// 		results := answer.Results
	// 		isRight := answer.IsRight.Key
	// 		if isRight == 0 {
	// 			marking = 1
	// 		}else {
	// 			marking = 2
	// 		}
	// 		score  := answer.Score
	// 		qu_num := answer.No
	// 		ti_id := answer.TiId
	// 		body_id, right_answer, qu_score, qu_type := GetInfoFromExam(exam_id, ti_id, db)
	// 		var ans string
	// 		if qu_type == 2 { // 多选题，将答案拼接在一起
	// 			ans = strings.Join(results, "")
	// 		} else { // 其他，只取最后一个
	// 			ans = results[len(results)-1]
	// 		}
	// 		stmt, err = tx.Prepare("UPDATE app_exam_result SET `answer`=?, `right_answer`=?, `marking`=?, `total_score`=?, `score`=?, `body_id`=?, `qu_type`=?, `ti_id`=?, `qu_num`=?, `del`=? WHERE `aes_id`=(SELECT aes_id FROM app_exam_student WHERE mongo_id=? AND user_no=?)")
	// 		PrintError("--> [exam_result - u] app_exam_result prepare failed : ", err)
	// 		_,	err = stmt.Exec(ans, right_answer, marking, qu_score, score, body_id, qu_type, ti_id, qu_num, del, mongo_id, user_no)
	// 		PrintError("--> [exam_result - u] update app_exam_result failed : ", err)
	// 	}
	// }
	// // 提交事务
	// err = tx.Commit()
	// PrintError("--> [exam_result - u] tx commit err : ", err)
	// fmt.Println("--> 更新成功")
// }