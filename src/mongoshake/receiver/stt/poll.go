package sst

import(
	"fmt"
	"reflect"
	"strconv"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"mongoshake/oplog"

	LOG "github.com/vinllen/log4go"
	"github.com/vinllen/mgo/bson"
)

/** 
* 投票 
* 
**/
func Poll_Handle(ol *oplog.PartialLog, db *sql.DB, base *sql.DB) {
	// 在线题库
	tk, err := sql.Open("mysql", "jonyuan:jonyuan135246!@#@tcp(39.106.108.171)/online_exercises_lib")
	if err != nil {
		LOG.Info(err)
	}
	defer tk.Close()
	fmt.Println("----------------------* " + ol.Namespace[15:] + " *-----------------")
	if ol.Operation == "i"{

		do_poll_insert(ol, db, base, tk)

	} else if ol.Operation == "u"{

		do_poll_update(ol,  db, base, tk)

	} else if ol.Operation == "d"{

		do_poll_delete(ol, db)

	} else {
		fmt.Println(ol.Operation)
		fmt.Println("\tQuery : ", ol.Query)
		fmt.Println("\tObject : ", ol.Object)
	}
}





/*****************************************************************
		更新 
*****************************************************************/
func do_poll_update(ol *oplog.PartialLog, db *sql.DB, base *sql.DB, tk *sql.DB) {
	query := ol.Query["_id"]
	qId, ok := query.(bson.ObjectId)
	if !ok {
		fmt.Println("--> [poll - u]断言失败：", ol.Query)
	}
	id := qId.Hex()
	o := ol.Object.Map()
	fmt.Println("更新 : ", id)

	set := o["$set"]
	if set != nil {
		// s := set.(bson.D)
		// m := s.Map()
		// fmt.Println(reflect.TypeOf(m))
		do_poll_update_part(id, set, db, base, tk)
	}else{
		do_poll_update_all(id, ol, db, base, tk)
	}
}


/*****************************************************************
		部分更新
*****************************************************************/
func do_poll_update_part(id string, set interface{}, db *sql.DB, base *sql.DB, tk *sql.DB) {
	s := set.(bson.D)
	m := s.Map()
	// 开启事务
	tx, err := db.Begin()
	PrintError("--> [poll - i] tx err : ", err)
	defer tx.Rollback()

	for k := range m {
		switch k {
		case "name":
			fmt.Println(k, " : ", m[k])
			ae_name := m[k]
			// app_exam
			stmt, err := tx.Prepare("UPDATE app_exam SET `ae_name`=? WHERE `stt_page_id`=?")
			PrintError("--> [poll - u] app_exam prepared failed : ", err)
			defer stmt.Close()
			_, err = stmt.Exec(ae_name, id)
			PrintError("--> [poll - u] app_exam update failed : ", err)
		 	// app_stt_page
			stmt, err = tx.Prepare("UPDATE `app_stt_page` SET `name`=? WHERE page_id=?")
			PrintError("--> [poll - u] app_stt_page prepared failed : ", err)
			_, err = stmt.Exec(ae_name, id)
			PrintError("--> [poll - u] app_stt_page update failed : ", err)

		case "topic":
			fmt.Println(k, " : ", m[k])
			ti_name := m[k]
			// app_stt_ti
			stmt, err := tx.Prepare("UPDATE `app_stt_ti` SET `ti_name`=? WHERE `ti_id`=?")
			PrintError("--> [poll - u] app_stt_ti prepared failed : ", err)
			defer stmt.Close()
			_, err = stmt.Exec(ti_name, id)
			PrintError("--> [poll - u] app_stt_ti update failed : ", err)

		case "bodyId":
			fmt.Println(k, " : ", m[k])
			body_id := (m[k]).(int)
			// body_id, _ := strconv.Atoi(m[k])
			// app_stt_page_detail
			stmt, err := tx.Prepare("UPDATE `app_stt_page_detail` SET `body_id`=? WHERE `page_id`=?")
			PrintError("--> [poll - u] app_stt_page_detail prepared failed : ", err)
			defer stmt.Close()
			_, err = stmt.Exec(body_id, id)
			PrintError("--> [poll - u] app_stt_page_detail update failed : ", err)
			if body_id > 0 {
				// 处理在线题库试题的 章节/知识点 关联关系
				DoQuKpRelation(body_id, db, tk, tx)
			}


		case "period":
			fmt.Println(k, " : ", m[k])
			year_D := (m[k]).(bson.D)
			year_m := year_D.Map()
			year := year_m["key"]
			// app_stt_page
			stmt, err := tx.Prepare("UPDATE `app_stt_page` SET `year`=? WHERE page_id=?")
			PrintError("--> [poll - u] app_stt_page prepared failed : ", err)
			defer stmt.Close()
			_, err = stmt.Exec(year, id)
			PrintError("--> [poll - u] app_stt_page update failed : ", err)

		case "answers":
			fmt.Println(k, " : ", m[k])
			answers := m[k]
			// app_stt_page_detail
			stmt, err := tx.Prepare("UPDATE `app_stt_page_detail` SET `answers`=? WHERE `page_id`=?")
			PrintError("--> [poll - u] app_stt_page_detail prepared failed : ", err)
			defer stmt.Close()
			_, err = stmt.Exec(answers, id)
			PrintError("--> [poll - u] app_stt_page_detail update failed : ", err)
			// app_stt_ti
			stmt, err = tx.Prepare("UPDATE `app_stt_ti` SET `ti_answers`=? WHERE `ti_id`=?")
			PrintError("--> [poll - u] app_stt_ti prepared failed : ", err)
			_, err = stmt.Exec(answers, id)
			PrintError("--> [poll - u] app_stt_ti update failed : ", err)

		case "type":
			fmt.Println(k, " : ", m[k])
			type_d := (m[k]).(bson.D)
			type_m := type_d.Map()
			qu_type := type_m["key"]
			// app_stt_page_detail
			stmt, err := tx.Prepare("UPDATE `app_stt_page_detail` SET `qu_type`=? WHERE `page_id`=?")
			PrintError("--> [poll - u] app_stt_page_detail prepared failed : ", err)
			defer stmt.Close()
			_, err = stmt.Exec(qu_type, id)
			PrintError("--> [poll - u] app_stt_page_detail update failed : ", err)

		case "options":
			fmt.Println(k, " : ", m[k])
			ops := (m[k].([]interface {}))
			for _, op := range ops {
				op_d := op.(bson.D)
				op_m := op_d.Map()
				key := op_m["key"]
				text := op_m["text"]
				// app_stt_item
				stmt, err := tx.Prepare("UPDATE `app_stt_item` SET `content`=? WHERE `ti_id`=? AND `key`=?")
				PrintError("--> [poll - u] app_stt_item prepared failed : ", err)
				defer stmt.Close()
				_, err = stmt.Exec(text, id, key)
				PrintError("--> [poll - u] app_stt_item update failed : ", err)
			}

		case "user":
			fmt.Println(k, " : ", m[k])
			user_d := (m[k]).(bson.D)
			user_m := user_d.Map()
			objId := user_m["$id"]
			oId := objId.(bson.ObjectId)
			user_id := oId.Hex()
			user_name, user_no 	:= GetUser(user_id, base)
			subjectId 			:= GetSubject(user_no, base)
			// app_exam
			stmt, err 	:= tx.Prepare("UPDATE `app_exam` SET `create_user_name`=?, `create_user_no`=?, `sub_id`=? WHERE `stt_page_id`=?")
			PrintError("--> [poll - u] app_exam prepared failed : ", err)
			defer stmt.Close()
			_, err 		 = stmt.Exec(user_name, user_no, subjectId, id)
			PrintError("--> [poll - u] app_exam update failed : ", err)
		 	// app_stt_page
			stmt, err = tx.Prepare("UPDATE `app_stt_page` SET `sub_id`=? WHERE page_id=?")
			PrintError("--> [poll - u] app_stt_page prepared failed : ", err)
			_, err = stmt.Exec(subjectId, id)
			PrintError("--> [poll - u] app_stt_page update failed : ", err)

		case "del":
			fmt.Println(k, " : ", m[k])
			del := m[k]
			// app_exam
			stmt, err 	:= tx.Prepare("UPDATE `app_exam` SET `del`=? WHERE `stt_page_id`=?")
			PrintError("--> [poll - u] app_exam prepared failed : ", err)
			defer stmt.Close()
			_, err = stmt.Exec(del, id)
			PrintError("--> [poll - u] app_exam update failed : ", err)
		 	// app_stt_page
			stmt, err = tx.Prepare("UPDATE `app_stt_page` SET `del`=? WHERE page_id=?")
			PrintError("--> [poll - u] app_stt_page prepared failed : ", err)
			_, err = stmt.Exec(del, id)
			PrintError("--> [poll - u] app_stt_page update failed : ", err)
			// app_stt_page_detail
			stmt, err = tx.Prepare("UPDATE `app_stt_page_detail` SET `del`=? WHERE `page_id`=?")
			PrintError("--> [poll - u] app_stt_page_detail prepared failed : ", err)
			_, err = stmt.Exec(del, id)
			PrintError("--> [poll - u] app_stt_page_detail update failed : ", err)
			// app_stt_ti
			stmt, err = tx.Prepare("UPDATE `app_stt_ti` SET `del`=? WHERE `ti_id`=?")
			PrintError("--> [poll - u] app_stt_ti prepared failed : ", err)
			_, err = stmt.Exec(del, id)
			PrintError("--> [poll - u] app_stt_ti update failed : ", err)
			// app_stt_item
			stmt, err = tx.Prepare("UPDATE `app_stt_item` SET `del`=? WHERE `ti_id`=?")
			PrintError("--> [poll - u] app_stt_item prepared failed : ", err)
			_, err = stmt.Exec(del, id)
			PrintError("--> [poll - u] app_stt_item update failed : ", err)

		default:
			fmt.Println(reflect.TypeOf(k), " : ", reflect.TypeOf(m[k]))
			fmt.Println(k, " : ", m[k])
		}
	}

	// 提交事务
	err = tx.Commit()
	PrintError("--> [poll - d] tx commit err : ", err)
	fmt.Println("--> 更新成功")
}


/*****************************************************************
		全量更新
*****************************************************************/
func do_poll_update_all(id string, ol *oplog.PartialLog, db *sql.DB, base *sql.DB, tk *sql.DB) {
	objectRaw, err := bson.Marshal(ol.Object)
	PrintError("", err)
	poll := new(Poll)
	if err := bson.Unmarshal(objectRaw, poll); err != nil {
		LOG.Crashf("unmarshal oplog[%v] failed[%v]", objectRaw, err)
		return
	}
	// 开启事务
	tx, err := db.Begin()
	PrintError("--> [poll-u] tx err : ", err)
	defer tx.Rollback()

	del 				:= poll.Del
	ae_name 			:= poll.Name
	user_id 			:= poll.User.Id.Hex()
	user_name, user_no 	:= GetUser(user_id, base)
	subjectId 			:= GetSubject(user_no, base)
	// app_exam
	stmt, err 	:= tx.Prepare("UPDATE `app_exam` SET `ae_name`=?, `create_user_name`=?, `create_user_no`=?, `sub_id`=?, `del`=? WHERE `stt_page_id`=?")
	PrintError("--> [poll - u] app_exam prepared failed : ", err)
	defer stmt.Close()
	_, err 		 = stmt.Exec(ae_name, user_name, user_no, subjectId, del, id)
	PrintError("--> [poll - u] app_exam update failed : ", err)

 	year := poll.Period.Key
 	// app_stt_page
	stmt, err = tx.Prepare("UPDATE `app_stt_page` SET `name`=?, `year`=?, `sub_id`=?, `del`=? WHERE page_id=?")
	PrintError("--> [poll - u] app_stt_page prepared failed : ", err)
	_, err = stmt.Exec(ae_name, year, subjectId, del, id)
	PrintError("--> [poll - u] app_stt_page update failed : ", err)

	var body_id int
	if poll.BodyId == "" {
		body_id = 0
	}else {
		body_id, _ = strconv.Atoi(poll.BodyId)
	}
	answers := poll.Answers
	qu_type := poll.Type.Key
	// app_stt_page_detail
	stmt, err = tx.Prepare("UPDATE `app_stt_page_detail` SET `body_id`=?, `ti_id`=?, `answers`=?, `qu_type`=?, `del`=? WHERE `page_id`=?")
	PrintError("--> [poll - u] app_stt_page_detail prepared failed : ", err)
	_, err = stmt.Exec(body_id, id, answers, qu_type, del, id)
	PrintError("--> [poll - u] app_stt_page_detail update failed : ", err)

	if body_id > 0 {
		// 处理在线题库试题的 章节/知识点 关联关系
		DoQuKpRelation(body_id, db, tk, tx)
	}

	ti_name := poll.Topic
	// app_stt_ti
	stmt, err = tx.Prepare("UPDATE `app_stt_ti` SET `ti_name`=?, `ti_answers`=?, `del`=? WHERE `ti_id`=?")
	PrintError("--> [poll - u] app_stt_ti prepared failed : ", err)
	_, err = stmt.Exec(ti_name, answers, del, id)
	PrintError("--> [poll - u] app_stt_ti update failed : ", err)

	if poll.Options != nil {
		for _, option := range poll.Options {
			key 	:= option.Key
			content := option.Text
			// app_stt_item
			stmt, err = tx.Prepare("UPDATE `app_stt_item` SET `content`=?, `del`=? WHERE `ti_id`=? AND `key`=?")
			PrintError("--> [poll - u] app_stt_item prepared failed : ", err)
			_, err = stmt.Exec(content, del, id, key)
			PrintError("--> [poll - u] app_stt_item update failed : ", err)
		}
	}

	// 提交事务
	err = tx.Commit()
	PrintError("--> [poll - u] tx commit err : ", err)
	fmt.Println("--> 更新成功")
}


/*****************************************************************
 		插入 
*****************************************************************/
func do_poll_insert(ol *oplog.PartialLog,  db *sql.DB,  base *sql.DB, tk *sql.DB) {
	objectRaw, err := bson.Marshal(ol.Object)
	PrintError("", err)
	poll := new(Poll)
	if err := bson.Unmarshal(objectRaw, poll); err != nil {
		LOG.Crashf("unmarshal oplog[%v] failed[%v]", objectRaw, err)
		return
	}
	fmt.Println("插入 : ", poll.Id)

	// 开启事务
	tx, err := db.Begin()
	PrintError("--> [poll - i] tx err : ", err)
	defer tx.Rollback()

	ae_name 			:= poll.Name
	user_id 			:= poll.User.Id.Hex()
	user_name, user_no 	:= GetUser(user_id, base)
	subjectId 			:= GetSubject(user_no, base)
	stt_page_id 		:= poll.Id.Hex()
	exam_status 		:= 2
	join_classes_type 	:= 0

	// app_poll
	stmt, err := tx.Prepare("INSERT INTO app_exam(stt_page_id, exam_status, join_classes_type, ae_name, create_user_name, create_user_no, sub_id, del) VALUES(?, ?, ?, ?, ?, ?, ?, 0)")
	PrintError("--> [poll - i] app_exam prepared failed : ", err)
	defer stmt.Close()
	_, err = stmt.Exec(stt_page_id, exam_status, join_classes_type, ae_name, user_name, user_no, subjectId)
	PrintError("--> [poll - i] app_exam insert failed : ", err)

	year := poll.Period.Key
	// app_stt_page
	stmt, err = tx.Prepare("INSERT INTO app_stt_page(page_id, name, year, sub_id, del) VALUES(?, ?, ?, ?, 0)")
	PrintError("--> [poll - i] app_stt_page prepared failed : ", err)
	_, err = stmt.Exec(stt_page_id, ae_name, year, subjectId)
	PrintError("--> [poll - i] app_stt_page insert failed : ", err)

	var body_id int
	if poll.BodyId == "" {
		body_id = 0
	}else {
		body_id, _ = strconv.Atoi(poll.BodyId)
	}
	answers := poll.Answers
	qu_type := poll.Type.Key

	// app_stt_page_detail
	stmt, err = tx.Prepare("INSERT INTO app_stt_page_detail(page_id, body_id, ti_id, answers, qu_type, del) VALUES(?, ?, ?, ?, ?, 0)")
	PrintError("--> [poll - i] app_stt_page_detail prepared failed : ", err)
	_, err = stmt.Exec(stt_page_id, body_id, stt_page_id, answers, qu_type)
	PrintError("--> [poll - i] app_stt_page_detail insert failed : ", err)

	if body_id > 0 {
		// 处理在线题库试题的 章节/知识点 关联关系
		DoQuKpRelation(body_id, db, tk, tx)
	}

	ti_name := poll.Topic
	// app_stt_ti
	stmt, err = tx.Prepare("INSERT INTO app_stt_ti(ti_id, ti_name, ti_answers, del) VALUES(?, ?, ?, 0)")
	PrintError("--> [poll - i] app_stt_ti prepared failed : ", err)
	_, err = stmt.Exec(stt_page_id, ti_name, answers)
	PrintError("--> [poll - i] app_stt_ti insert failed : ", err)

	if poll.Options != nil {
		for _, option := range poll.Options {
			key := option.Key
			content := option.Text

			// app_stt_item
			stmt, err = tx.Prepare("INSERT INTO app_stt_item(`ti_id`, `key`, `content`, `del`) VALUES(?, ?, ?, 0)")
			PrintError("--> [poll - i] app_stt_item prepared failed : ", err)
			_, err = stmt.Exec(stt_page_id, key, content)
			PrintError("--> [poll - i] app_stt_item insert failed : ", err)
		}
	}

	// 提交事务
	err = tx.Commit()
	PrintError("--> [poll - d] tx commit err : ", err)
	fmt.Println("--> 插入成功")
}


/*****************************************************************
 		删除 
*****************************************************************/
func do_poll_delete(ol *oplog.PartialLog, db *sql.DB) {
	objectRaw, err := bson.Marshal(ol.Object)
	PrintError("", err)
	objId := new(ObjId)
	if err := bson.Unmarshal(objectRaw, objId); err != nil {
		LOG.Crashf("unmarshal oplog[%v] failed[%v]", objectRaw, err)
		return
	}
	id := objId.Id.Hex()
	fmt.Println("删除 : ", id)

	// 开启事务
	tx, err := db.Begin()
	PrintError("--> [poll - d] tx err : ", err)
	defer tx.Rollback()

	// app_exam
	stmt, err := tx.Prepare("UPDATE app_exam SET `del`=1 WHERE stt_page_id=?")
 	PrintError("--> [poll - d] app_exam prepared failed : ", err)
 	defer stmt.Close()
 	_, err = stmt.Exec(id)
 	PrintError("--> [poll - d] app_exam update failed : ", err)

 	// app_stt_page
	stmt, err = tx.Prepare("UPDATE app_stt_page SET `del`=1 WHERE page_id=?")
	PrintError("--> [poll - d] app_stt_page prepared failed : ", err)
	_, err = stmt.Exec(id)
	PrintError("--> [poll - d] app_stt_page insert failed : ", err)
	
	// app_stt_page_detail
	stmt, err = tx.Prepare("UPDATE app_stt_page_detail SET `del`=1 WHERE page_id=?")
	PrintError("--> [poll - d] app_stt_page_detail prepared failed : ", err)
	_, err = stmt.Exec(id)
	PrintError("--> [poll - d] app_stt_page_detail insert failed : ", err)

	// app_stt_ti
	stmt, err = tx.Prepare("UPDATE app_stt_ti SET `del`=1 WHERE ti_id=?")
	PrintError("--> [poll - d] app_stt_ti prepared failed : ", err)
	_, err = stmt.Exec(id)
	PrintError("--> [poll - d] app_stt_ti insert failed : ", err)
	
	// app_stt_item
	stmt, err = tx.Prepare("UPDATE app_stt_item SET `del`=1 WHERE ti_id=?")
	PrintError("--> [poll - d] app_stt_item prepared failed : ", err)
	_, err = stmt.Exec(id)
	PrintError("--> [poll - d] app_stt_item insert failed : ", err)
	
	// 提交事务
	err = tx.Commit()
	PrintError("--> [poll - d] tx commit err : ", err)
	fmt.Println("--> 删除成功")
}


// 打印
func printPoll(poll *Poll) {
	fmt.Println("id : \t\t", 		poll.Id.Hex())
	fmt.Println("bodyId : \t\t", 	poll.BodyId)
	fmt.Println("source : \t", 		poll.Source)
	fmt.Println("type : \t\t", 		poll.Type)
	fmt.Println("name : \t\t", 		poll.Name)
	fmt.Println("topic : \t\t", 	poll.Topic)
	fmt.Println("options : \t")
	if poll.Options != nil {
		for i, _ := range poll.Options {
			fmt.Println("\t", *poll.Options[i])
		}
	}
	fmt.Println("answers : \t", 	poll.Answers)
	fmt.Println("duration : \t", 	poll.Duration)
	fmt.Println("isAnonymous : \t", poll.IsAnonymous)
	fmt.Println("period : \t", 		poll.Period)
	fmt.Println("term : \t\t", 		poll.Term)
	fmt.Println("userType : \t", 	poll.UserType)
	fmt.Println("Classify : \t", 	poll.Classify)
	fmt.Println("createTime : \t", 	poll.CreateTime)
	fmt.Println("updateTime : \t", 	poll.UpdateTime)
	fmt.Println("del : \t\t", 		poll.Del)
	fmt.Println("version : \t", 	poll.Version)
	fmt.Println("user : \t\t", 		poll.User.Id.Hex())
	fmt.Println("answMnt : \t", 	poll.AnswMnt)
	fmt.Println("isAllLower : \t", 	poll.IsAllLower)
	fmt.Println("isModify : \t", 	poll.IsModify)
	fmt.Println("clientRes : \t", 	poll.ClientRes)
	fmt.Println("cPollId : \t", 	poll.CPollId)
	fmt.Println("pollLs : \t")
	if poll.PollLs != nil {
		fmt.Println("\t grade : \t", 	poll.PollLs.Grade)
		fmt.Println("\t bussType : \t", poll.PollLs.BussType)
		fmt.Println("\t classes : \t", 	poll.PollLs.Classes.Id.Hex())
		fmt.Println("\t banCi : \t", 	poll.PollLs.BanCi.Id.Hex())
	}
	fmt.Println("uploadTime : \t", 		poll.UploadTime)
	fmt.Println("gridFsId : \t", 		poll.GridFsId)
}